"""GIT plugin for sopel, created for my specific use cases.

Copyright 2015, Eric l Sammons
"""

from sopel.config.types import StaticSection, ValidatedAttribute
from sopel import module
from sopel.module import rule, commands, example, interval
from sopel.tools import Identifier
import git
import os

global max

# We could, may move this to a bot.configure; however, for the time
# it will suffice here.
# Used to limit the amount of spam when responding to speific queries.
max = 5

def _clone_repo(path, repo):
    """
    Clone a git repository based on URL provided.  For the purpose of our bot
    we'll only clone into a fixed location, the root of which is where you
    started the bot, so be careful to start the bot from the same location or
    you'll find multiple copies of the same repositories.

    :param path:
    :param repo:
    :return:
    """
    if not os.path.exists(path):
        git.Git('.').clone(repo)

def _get_commits(repo, sha=None, n=5):
    """
    Get commits by sha or not, return up to 5 by default or more if desired.
    :param repo: repo as a path to the local clone.
    :param sha: The sha for a specific commit.
    :param n: The number of requested commits.
    :return:
    """
    if sha != None:
        return repo.commit(sha)
    else:
        return list(repo.iter_commits('master'))[:n]

#@module.interval(300)
def _pull(repo):
    """
    Pull and fetch from the upstream, syncs the local repos to ensure latest information.

    :param repo:
    :return:
    """
    repo.git.pull()

@module.commands('subscribe')
@module.example('subscribe https://git.url, https://git2.url, https://git3.url')
def subscribe(bot, trigger):
    """Subscribe to one or more public git repositories.

    Separate subscriptions with a comma.
    Note: Subscribe should be run as your registered irc nick.
    """
    nick = trigger.nick
    repos = trigger.group(2)

    try:
        bot.db.set_nick_value(nick, 'git_subscribe', repos)
        return bot.reply("%s is now subscribed." % nick)
    except TypeError:
        return bot.reply('Are you using your regular nick? and Did you provide urls separated by comma?')


@module.commands('subscriptions')
def get_subscriptions(bot, trigger):
    """Return a list of current subscriptions.

    Will default to use your nick otherwise you can specify a nick to see
    what others are subscribed to or if you happen to be using a different nick.
    :param bot:
    :param trigger:
    :return:
    """
    if trigger.group(2) == None:
        nick = trigger.nick
    else:
        nick = trigger.group(2)
    try:
        subscriptions = bot.db.get_nick_value(nick, 'git_subscribe').split(",")
    except AttributeError:
        return bot.reply("Oops that nick does not exist in our DB.")

    if len(subscriptions) > 0:
        for s in subscriptions:
            s = s.strip()
            message = s.rsplit('/',1)[-1].split('.')[0] + " : " + s
            if len(subscriptions) >= max:
                bot.reply(message, trigger.nick)
            else:
                bot.reply(message)
    else:
        return bot.reply("It looks like you have no subscriptions, subscribe first.")

    return


@module.commands('clone', 'pull')
def clone_repo(bot, trigger):
    """Clones and pulls (updates) your subscribed repos.
    """
    nick = trigger.nick
    repos = bot.db.get_nick_value(nick, 'git_subscribe').split(',')

    for url in repos:
        url = url.strip()
        local_repo = url.rsplit('/',1)[-1].split('.')[0]
        path = os.path.join('.', local_repo)

        #try:
        _clone_repo(path, url)
        bot.reply('Local repository %s has been cloned.' % local_repo)
        #except:
        #    return bot.reply('Something went wrong with cloning %s.' % local_repo)
    return


@module.commands("commits?")
@module.example("commits qpid-proton sha:fca557871e7 count:5")
def get_commit(bot, trigger):
    """Get a git commit for a given repository by sha.
    :param repo: required field, short name of repo.
    :param sha: written as sha:sha-value, is optional.
    :param count: written as count:num, number of commits to return, default is 5.
    """
    #repo_path = os.path.join(".", trigger.group(3))
    sha = None
    count = 1
    repo_name = trigger.group(3)

    for item in trigger.groups()[3:6]:
        if item != None:
            key, value = item.split(':')
            if key == "sha":
                sha = value
            elif key == 'count':
                count = int(value)

    repo_path = os.path.join('.', repo_name)

    if os.path.exists(repo_path):
        repo = git.Repo(repo_path)
        try:
            commits = _get_commits(repo, sha, count)
        except TypeError:
            return bot.reply("Couldn't find what you were looking for.")

        if sha:
            commit = commits
            message = "[" + repo_name + " | " + commit.author.name + "] " + \
                commit.message.split('\n')[0]
            return bot.reply(message)

        for commit in commits:
            message = "[" + repo_name + " | " + commit.author.name + "] " + \
                commit.message.split('\n')[0]
            if len(commits) < max:
                bot.reply(message)
            else:
                bot.reply(message, trigger.nick)
    return

    # if os.path.exists(repo_path):
    #     repo = git.Repo(repo_path)
    # else:
    #     return bot.reply("Can not set repo, check subscriptions or raise with admin.")
    #
    # try:
    #     return bot.reply("%s" % repo.commit(sha))
    # except:
    #     return bot.reply("Could not find the commit.")






## TODO
# Figure out how to have this module periodically poll the repos.
# Establish a default git repo path, so that nicks that subscribe to same can use the already cloned.
#
